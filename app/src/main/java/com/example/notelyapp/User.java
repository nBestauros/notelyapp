package com.example.notelyapp;

import java.util.ArrayList;
import java.util.UUID;

import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class User extends RealmObject {
    @PrimaryKey
    private String id;
    private String name;
    private RealmList<Note> notes;

    public User(){
        name = "";
        id = UUID.randomUUID().toString();
        notes = new RealmList<Note>();
    }
    public String getId(){return id;}
    public String getName(){
        return name;
    }
    public void addNote(Note n){
        notes.add(n);
    }
    public void setName(String n){
        name = n;
    }
    public void removeNote(Note n){
        notes.remove(n);
    }
    public RealmList<Note> getNotes(){
        return notes;
    }
    public void setNotes(RealmList<Note> n){
        notes = n;
    }
}
