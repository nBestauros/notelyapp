package com.example.notelyapp;


import android.app.PendingIntent;

import java.util.Calendar;
import java.util.Date;
import java.util.UUID;

import io.realm.RealmObject;

public class Note extends RealmObject {
    private String id;
    private String name;
    private String content;
    //public PendingIntent pendingIntent;
    public String date = "";
    public Note(){
        name = "note name";
        content = "note content";
        id = UUID.randomUUID().toString();
    }
    public Note(String n, String c){
        name = n;
        content = c;
        id = UUID.randomUUID().toString();
    }
    public String getID(){
        return id;
    }
    public void changeName(String n){
        name = n;
    }
    public void changeContent(String c){
        content = c;
    }
    public String getName(){
        return name;
    }
    public String getContent(){
        return content;
    }
}
