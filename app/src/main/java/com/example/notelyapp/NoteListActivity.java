package com.example.notelyapp;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

import io.realm.Realm;
import io.realm.RealmConfiguration;
import io.realm.RealmList;
import io.realm.RealmQuery;
import io.realm.RealmResults;

public class NoteListActivity extends AppCompatActivity {
    Realm realm;
    LinearLayoutManager layoutManager;
    RecyclerView recyclerView;
    NoteAdapter noteAdapter;
    String username;
    Note testNote;

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.note_list_toolbar, menu);
        return true;
    }

    private final int EDIT_CODE = 2;

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        // Check which request we're responding to
        if (requestCode == EDIT_CODE) {
            // Make sure the request was successful
            if (resultCode == RESULT_OK) {
                noteAdapter.notifyItemInserted(realm.where(User.class).equalTo("name",username).findFirst().getNotes().size()-1);
                noteAdapter.notifyDataSetChanged();
                //Toast.makeText(this, "making new note", Toast.LENGTH_SHORT).show();
            }
            noteAdapter.notifyItemInserted(realm.where(User.class).equalTo("name",username).findFirst().getNotes().size()-1);
            noteAdapter.notifyDataSetChanged();
        }
    }

    private void createNote(){
        Context context = this;
        Intent intent = new Intent(context, NoteReaderActivity.class);
        intent.putExtra("username",username);
        intent.putExtra("EDIT_CODE",EDIT_CODE);
        startActivityForResult(intent,EDIT_CODE);


    }
    @Override
    protected void onResume()
    {
        super.onResume();
        noteAdapter.notifyDataSetChanged();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.item_add:
                createNote();

                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        Realm.init(this);

        realm = Realm.getDefaultInstance();
/*        realm.beginTransaction();

        User test1 = realm.createObject(User.class, new User().getId());
        RealmList<Note> tempList = test1.getNotes();
        test1.setName("test1");


        testNote = new Note();
        tempList.add(testNote);
        Note testNote1 = new Note("noteName", "note content");
        tempList.add(testNote1);
        testNote = new Note("noteName", "content changed again");
        tempList.add(testNote);
        testNote = new Note("noteName", "content");
        tempList.add(testNote);
        testNote = new Note("noteName", "da33");
        tempList.add(testNote);
        testNote = new Note("noteName", "testing!");
        tempList.add(testNote);

        realm.copyToRealmOrUpdate(tempList);
        test1.setNotes(tempList);



        realm.commitTransaction();*/



        super.onCreate(savedInstanceState);
        Intent intent = getIntent();
        username = intent.getStringExtra(MainActivity.USERNAME);

        setContentView(R.layout.activity_display_note);
        recyclerView = findViewById(R.id.recyclerView);

        layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);


        //gets a user, then gets their notes
        RealmList<Note> notes;


        User temp = realm.where(User.class).equalTo("name",username).findFirst();

        if(temp!=null){
            notes = temp.getNotes();
            noteAdapter = new NoteAdapter(notes,username);


            recyclerView.setAdapter(noteAdapter);
        }
        else{
            Toast.makeText(this, "Go back and try: test1", Toast.LENGTH_LONG).show();
        }


        //TODO: figure out where to use realm.close();
    }
}
