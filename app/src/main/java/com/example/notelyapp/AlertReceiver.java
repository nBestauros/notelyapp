package com.example.notelyapp;

import android.app.Notification;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationManagerCompat;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import io.realm.Realm;
import io.realm.RealmList;

import static com.example.notelyapp.BaseApp.CHANNEL_1_ID;

public class AlertReceiver extends BroadcastReceiver {
    NotificationManagerCompat notificationManager;
    Realm realm;

    @Override
    public void onReceive(Context context, Intent intent) {
        realm = Realm.getDefaultInstance();

        int index = intent.getIntExtra("index",-1);
        String user = intent.getStringExtra("username");
        if(realm.where(User.class).equalTo("name",user).findFirst()!=null)
        {
            int editCode = intent.getIntExtra("code",-1);
            //Toast.makeText(context, user, Toast.LENGTH_SHORT).show();
            Log.d("userName","Username "+user);
            if(editCode==2){
                Log.d("hi",index+"");
                index = realm.where(User.class).equalTo("name",user).findFirst().getNotes().size()-1;
                Log.d("hi",index+"");
            }
            //Note note = realm.where(User.class).equalTo("name",user).findFirst().getNotes().get(index);
            //note.date="";
            User grabbed = realm.where(User.class).equalTo("name",user).findFirst();
            RealmList<Note> list = grabbed.getNotes();
            Log.d("name",intent.getStringExtra("name"));
            int index2 = -1;
            for(int i = 0;i<list.size();i++){
                if(list.get(i).getID().equals(intent.getStringExtra("id")))
                    index2=i;
                Log.d("hi","in2"+index2);
            }
            if(index2>-1) {
                realm.beginTransaction();
                Note note = list.get(index2);
                note.date = "";
                list.set(index2, note);
                realm.copyToRealmOrUpdate(grabbed);
                realm.commitTransaction();


                String n = intent.getStringExtra("name");
                notificationManager = NotificationManagerCompat.from(context);
                Notification notification = new NotificationCompat.Builder(context, CHANNEL_1_ID)
                        .setSmallIcon(R.drawable.ic_launcher_foreground)
                        .setContentTitle("You need to review the note: " + note.getName())
                        .setPriority(NotificationCompat.PRIORITY_DEFAULT)
                        .build();

                notificationManager.notify(1, notification);
            }
        }







    }
}
