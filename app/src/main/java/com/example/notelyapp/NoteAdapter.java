package com.example.notelyapp;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.lang.reflect.Array;
import java.util.ArrayList;

import io.realm.RealmList;

public class NoteAdapter extends RecyclerView.Adapter<NoteAdapter.ViewHolder> {
    private RealmList<Note> list;
    private String username;

    public NoteAdapter(RealmList<Note> list,String u)
    {
            this.list = list;
            username = u;
        }

        public static class ViewHolder extends RecyclerView.ViewHolder {
            public TextView name;
            public TextView content;

            public ViewHolder(RelativeLayout view) {
                super(view);
                name = view.findViewById(R.id.name);
                content = view.findViewById(R.id.content);
            }
        }

    @Override
    public NoteAdapter.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                     int viewType) {
        // create a new view
        RelativeLayout v = (RelativeLayout) LayoutInflater.from(parent.getContext())
                .inflate(R.layout.note_item, parent, false);
        ViewHolder vh = new ViewHolder(v);

        return vh;
    }



    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        // - get element from your dataset at this position
        // - replace the contents of the view with that element
        holder.name.setText(list.get(position).getName());
        holder.content.setText(list.get(position).getContent());
        final int itemPosition = holder.getAdapterPosition();

        final int EDIT_CODE = 3;
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //tell NoteReaderActivity what the position is
                Context context = v.getContext();
                Intent intent = new Intent(context, NoteReaderActivity.class);
                //Toast.makeText(context, ""+itemPosition, Toast.LENGTH_SHORT).show();
                intent.putExtra("position",itemPosition);
                intent.putExtra("EDIT_CODE",EDIT_CODE);
                intent.putExtra("username",username);
                context.startActivity(intent);
                //TODO: how do i use a startactivityforresult here?  i want to wait for the notereader to be done and to tell this part to notifydatasetchanged after its been modified in reader
                //current behavior is to just update on the click so we need to click again for it to update after we saved.
                notifyDataSetChanged();
            }
        });

    }

    @Override
    public int getItemCount() {
        return list.size();
    }
    }
