package com.example.notelyapp;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import io.realm.Realm;
import io.realm.RealmConfiguration;
import io.realm.RealmList;
import io.realm.RealmResults;

public class MainActivity extends AppCompatActivity{
    public static final String USERNAME = "com.example.notelyapp.USERNAME";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Realm.init(this);
        RealmConfiguration config = new RealmConfiguration.Builder()
                .deleteRealmIfMigrationNeeded()
                .build();
        Realm.setDefaultConfiguration(config);
        displayUsers();
    }

    /** Called when the user taps the Login button */
    public void sendMessage(View view) {
        // Do something in response to button
        Realm realm = Realm.getDefaultInstance();
        Intent intent = new Intent(this, NoteListActivity.class);
        EditText editText = (EditText) findViewById(R.id.editText);
        String user = editText.getText().toString();
        User temp = realm.where(User.class).equalTo("name",user).findFirst();
        if(temp!=null){
            intent.putExtra(USERNAME, user);
            startActivity(intent);
        }
        else{
            Toast.makeText(this, "Invalid Account", Toast.LENGTH_LONG).show();
        }
    }

    public void newUser(View view){
        Realm realm = Realm.getDefaultInstance();
        EditText editText = (EditText) findViewById(R.id.newUserField);
        String user = editText.getText().toString();
        if(!user.equals("")){
            realm.beginTransaction();
            User test1 = realm.createObject(User.class, new User().getId());
            test1.setName(user);
            editText.setText("");
            realm.commitTransaction();
            Toast.makeText(this, "User: "+user+" added", Toast.LENGTH_SHORT).show();
        }
        else{
            Toast.makeText(this, "User field can't be blank.", Toast.LENGTH_SHORT).show();
        }
        displayUsers();
        editText.setText("");
    }

    public void deleteUser(View view){
        Realm realm = Realm.getDefaultInstance();
        EditText editText = (EditText) findViewById(R.id.newUserField);
        String user = editText.getText().toString();
        if(!user.equals("")){
            realm.beginTransaction();
            realm.where(User.class).equalTo("name",user).findAll().deleteAllFromRealm();

            realm.commitTransaction();
            Toast.makeText(this, "User: "+user+" removed", Toast.LENGTH_SHORT).show();
        }
        else{
            Toast.makeText(this, "User field can't be blank.", Toast.LENGTH_SHORT).show();
        }
        displayUsers();
        editText.setText("");
    }
    public void displayUsers(){
        Realm realm = Realm.getDefaultInstance();
        RealmResults<User> userList = realm.where(User.class).findAll();
        String names = "Users:";
        for(int i=0; i<userList.size();i++){
            names+=" "+userList.get(i).getName();
        }
        TextView userView = (TextView) findViewById(R.id.userView);
        userView.setText(names);
    }
}