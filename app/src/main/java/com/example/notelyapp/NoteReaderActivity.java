package com.example.notelyapp;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.TimePicker;

import java.text.DateFormat;
import java.util.Calendar;

import io.realm.Realm;
import io.realm.RealmList;


public class NoteReaderActivity extends AppCompatActivity implements TimePickerDialog.OnTimeSetListener{

    Realm realm;
    int index;
    int editCode;
    String user;
    String userName;

    String tempDate;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_note_reader);
        realm = Realm.getDefaultInstance();
        Intent intent = getIntent();
        index = intent.getIntExtra("position",-1);
        editCode = intent.getIntExtra("EDIT_CODE",-1);
        user = intent.getStringExtra("username");
        userName = intent.getStringExtra("username");
        EditText title = (EditText) findViewById(R.id.titleEdit);
        EditText content = (EditText) findViewById(R.id.contentEdit);
        RealmList<Note> list = realm.where(User.class).equalTo("name",user).findFirst().getNotes();
        if(editCode==3){
            title.setText(list.get(index).getName());
            content.setText(list.get(index).getContent());
        }
        else if(editCode==2){
            title.setText("Title");
            content.setText("Content");
        }
        updateTimeText();
    }

    @Override
    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
        //TextView tv = (TextView)findViewById(R.id.time);
        //tv.setText("Hour: "+hourOfDay+" Minute: " + minute);
        Calendar c = Calendar.getInstance();
        c.set(Calendar.HOUR_OF_DAY,hourOfDay);
        c.set(Calendar.MINUTE,minute);
        c.set(Calendar.SECOND,0);

        updateTimeText(c);
        startAlarm(c);
    }

    private void updateTimeText(){
        User grabbed = realm.where(User.class).equalTo("name",user).findFirst();
        RealmList<Note> list = grabbed.getNotes();
        if(editCode==3){
            Note n = list.get(index);
            if(!n.date.equals(""))
            {
                TextView tv = (TextView)findViewById(R.id.time);
                String timeText = "Reminder: "+ n.date;
                tv.setText(timeText);
            }

        }


    }
    private void updateTimeText(Calendar c){
        String timeText = "Reminder: "+ DateFormat.getTimeInstance(DateFormat.SHORT).format(c.getTime());

        TextView tv = (TextView)findViewById(R.id.time);
        tv.setText(timeText);
    }


    private void startAlarm(Calendar c){
        AlarmManager alarmManager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
        EditText titleText = (EditText) findViewById(R.id.titleEdit);

        Intent intent = new Intent(this, AlertReceiver.class);
        if(editCode==3)
            intent.putExtra("name", realm.where(User.class).equalTo("name",user).findFirst().getNotes().get(index).getName());
        if(editCode==3)
            intent.putExtra("index",index);
        else
            intent.putExtra("index",realm.where(User.class).equalTo("name",user).findFirst().getNotes().size());
        if(editCode==2)
            intent.putExtra("name",titleText.getText().toString());
        if(editCode==3){
            intent.putExtra("id", realm.where(User.class).equalTo("name",user).findFirst().getNotes().get(index).getID());
        }

        intent.putExtra("code",editCode);
        Log.d("NRA", userName);
        intent.putExtra("username", userName);


        tempDate = DateFormat.getTimeInstance(DateFormat.SHORT).format(c.getTime());

        if(editCode==3){
            realm.beginTransaction();
            Note n = realm.where(User.class).equalTo("name",user).findFirst().getNotes().get(index);
            n.date=DateFormat.getTimeInstance(DateFormat.SHORT).format(c.getTime());
            User grabbed = realm.where(User.class).equalTo("name",user).findFirst();
            RealmList<Note> list = grabbed.getNotes();
            list.set(index,n);
            realm.copyToRealmOrUpdate(grabbed);
            realm.commitTransaction();
        }

        if(editCode==2){
            realm.beginTransaction();
            User grabbed = realm.where(User.class).equalTo("name",user).findFirst();
            RealmList<Note> list = grabbed.getNotes();
            editCode = intent.getIntExtra("EDIT_CODE",-1);
            Note note = new Note();
            titleText = (EditText) findViewById(R.id.titleEdit);
            note.changeName(titleText.getText().toString());
            EditText bodyText = (EditText) findViewById(R.id.contentEdit);
            note.changeContent(bodyText.getText().toString());

            if(tempDate!=null)
                note.date = tempDate;
            list.add(note);
            grabbed.setNotes(list);
            realm.copyToRealmOrUpdate(grabbed);
            realm.commitTransaction();
            intent.putExtra("id",note.getID());
            PendingIntent pendingIntent = PendingIntent.getBroadcast(this, 1, intent, 0);
            alarmManager.set(AlarmManager.RTC_WAKEUP, c.getTimeInMillis(),pendingIntent);
            finish();
        }
        PendingIntent pendingIntent = PendingIntent.getBroadcast(this, 1, intent, 0);
        alarmManager.set(AlarmManager.RTC_WAKEUP, c.getTimeInMillis(),pendingIntent);
        //enableNotification(pendingIntent);
    }
    public void startTimePicker(View v){
        DialogFragment timePicker = new TimePickerFragment();
        timePicker.show(getSupportFragmentManager(), "time picker");
    }

    public void cancelAlarm(View v){
        AlarmManager alarmManager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
        Intent intent = new Intent(this, AlertReceiver.class);
        intent.putExtra("name", realm.where(User.class).equalTo("name",user).findFirst().getNotes().get(index).getName());
        intent.putExtra("index",index);
        Log.d("NRA", userName);
        intent.putExtra("username", userName);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(this, 1, intent, 0);

        realm.beginTransaction();
        Note n = realm.where(User.class).equalTo("name",user).findFirst().getNotes().get(index);
        n.date="";
        User grabbed = realm.where(User.class).equalTo("name",user).findFirst();
        RealmList<Note> list = grabbed.getNotes();
        list.set(index,n);
        realm.copyToRealmOrUpdate(grabbed);
        realm.commitTransaction();

        tempDate = null;



        alarmManager.cancel(pendingIntent);
        TextView tv = (TextView)findViewById(R.id.time);
        tv.setText("");
        //disableNotification();
    }

    public void enableNotification(PendingIntent pi){
        RealmList<Note> list = realm.where(User.class).equalTo("name",user).findFirst().getNotes();
        Note n = list.get(index);
        //n.pendingIntent=pi;
        list.set(index,n);
        User grabbed = realm.where(User.class).equalTo("name",user).findFirst();
        grabbed.setNotes(list);
        realm.copyToRealmOrUpdate(grabbed);
    }

    public void disableNotification(){
        RealmList<Note> list = realm.where(User.class).equalTo("name",user).findFirst().getNotes();
        Note n = list.get(index);
        //n.pendingIntent=null;
        list.set(index,n);
        User grabbed = realm.where(User.class).equalTo("name",user).findFirst();
        grabbed.setNotes(list);
        realm.copyToRealmOrUpdate(grabbed);
    }



    public void sendMessage(View view) {
        // Do something in response to button
        // 3 is the edit of an existing note
        realm.beginTransaction();
        User grabbed = realm.where(User.class).equalTo("name",user).findFirst();
        RealmList<Note> list = grabbed.getNotes();
        Intent intent = getIntent();
        editCode = intent.getIntExtra("EDIT_CODE",-1);
        if(editCode==3){
            Note note = list.get(index);
            EditText titleText = (EditText) findViewById(R.id.titleEdit);
            note.changeName(titleText.getText().toString());
            EditText bodyText = (EditText) findViewById(R.id.contentEdit);
            note.changeContent(bodyText.getText().toString());

            list.set(index,note);
            grabbed.setNotes(list);


        }
        //2 is the edit of a new note;
        if(editCode==2){

            Note note = new Note();
            EditText titleText = (EditText) findViewById(R.id.titleEdit);
            note.changeName(titleText.getText().toString());
            EditText bodyText = (EditText) findViewById(R.id.contentEdit);
            note.changeContent(bodyText.getText().toString());

            if(tempDate!=null)
                note.date = tempDate;
            list.add(note);
            grabbed.setNotes(list);

        }






        realm.copyToRealmOrUpdate(grabbed);
        realm.commitTransaction();
        finish();
    }
    public void deleteButton(View view){
        if(editCode==2){
            finish();
        }
        if(editCode!=2){
            realm.beginTransaction();
            User grabbed = realm.where(User.class).equalTo("name",user).findFirst();
            RealmList<Note> list = grabbed.getNotes();
            list.remove(index);
            grabbed.setNotes(list);
            realm.copyToRealmOrUpdate(grabbed);
            realm.commitTransaction();
            finish();
        }

    }


}
