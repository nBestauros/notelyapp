package com.example.notelyapp;

import java.util.ArrayList;

import io.realm.Realm;
import io.realm.RealmResults;

public class NoteHelper {
    Realm realm;
    public NoteHelper(Realm realm) {
        this.realm = realm;
    }

    //adds new note
    public void save(final Note note)
    {
        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {

                Note n=realm.copyToRealm(note);

            }
        });

    }

    //READ
    public ArrayList<String> retrieve()
    {
        ArrayList<String> noteNames=new ArrayList<>();
        RealmResults<Note> notes=realm.where(Note.class).findAll();

        for(Note s:notes)
        {
            noteNames.add(s.getName());
        }

        return noteNames;
    }
}