package com.example.notelyapp;

import android.app.Application;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.os.Build;

public class BaseApp extends Application {

    public static final String CHANNEL_1_ID = "reminder";
    @Override
    public void onCreate(){
        super.onCreate();

        createNotificationChannels();

    }

    private void createNotificationChannels(){
        if(Build.VERSION.SDK_INT>= Build.VERSION_CODES.O){
            NotificationChannel channel1 = new NotificationChannel(
                    CHANNEL_1_ID,
                    "Reminders",
                    NotificationManager.IMPORTANCE_DEFAULT
            );
            channel1.enableLights(true);
            channel1.setDescription("Allows Notely to display reminders to review notes.");

            NotificationManager manager = getSystemService(NotificationManager.class);
            manager.createNotificationChannel(channel1);
        }
    }
}
